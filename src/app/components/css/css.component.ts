import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-css',
  template: `
    <p>
      El estilo solo se aplica a este parrafo
    </p>
  `,
  styles: [`
    p {
      color:red;
      font-size: 15px
    }
  `]
})
export class CssComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }


  // http://localhost:4200//
  // https://www.udemy.com/angular-2-fernando-herrera/learn/lecture/6464872#overview
  // https://bitbucket.org/UK_dev_masters/06-miscelaneos/src
  // https://angular.io/api/common/NgStyle

}

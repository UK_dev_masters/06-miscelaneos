import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  template: `
    <!-- <p [ngStyle]="{'font-size': tamano + 'px', 'color': 'red'}">  -->
     <p [style.fontSize.px]="tamano">
      Hola mundo esta es una etiqueta html
    </p>

    <button class="btn btn-primary" (click)="tamano = tamano + 5">
      <i class="fa fa-x3 fa-plus"></i>
    </button>
    
    <button class="btn btn-primary" (click)="tamano = tamano - 5">
      <i class="fa fa-x3 fa-minus"></i>
    </button>
  `,
  styles: []
})
export class NgStyleComponent implements OnInit {
  
  tamano:number = 10;
  constructor() { }

  ngOnInit() {
  }

}

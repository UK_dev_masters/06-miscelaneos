import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  constructor( private el: ElementRef ) {
    console.log('directiva llamada');
    // el.nativeElement.style.backgroundColor = "yellow";
  }

  @Input("appResaltado") nuevoColor:string; //variable que recoje el parámetro que se envia desde el html

  @HostListener('mouseenter') mouseEntro(){
    this.resaltarColor(this.nuevoColor || 'yellow');
  }
  @HostListener('mouseleave') mouseSalio(){
    this.resaltarColor(null);
  }

  private resaltarColor(color:string){
    this.el.nativeElement.style.backgroundColor = color;
  }

}
